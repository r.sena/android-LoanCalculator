package ryansenna.com.loancalculator;

/**
 * Created by Railanderson Sena on 2016-09-07.
 */
public class LoanCalculator {

    private double monthlyRate;
    private double time;
    private double loanAmount;

    /**
     *
     * @param loanAmount
     * @param monthlyRate
     * @param time
     */
    public LoanCalculator(double loanAmount, double monthlyRate, double time) {

        setLoanAmount(loanAmount);
        setMonthlyRate(monthlyRate);
        setTime(time);
    }


    /**
     * LIST OF GETTERS AND SETTERS
     */

    /**
     *
     * @return
     */
    public double getLoanAmount() {
        return loanAmount;
    }

    /**
     *
     * @return
     */
    public double getMonthlyRate() {
        return monthlyRate;
    }

    /**
     *
     * @return
     */
    public double getTime() {
        return time;
    }

    /**
     * The method calculates the monthly payment of a loan.
     * @return monthlyPay
     */
    public double getMonthlyPayment() {
        return loanAmount * (monthlyRate / (1 - (Math.pow((1 + monthlyRate), -time))));
    }

    /**
     * The method calculates the total payment plus interests.
     * @return totalPay
     */
    public double getTotalPayment(){
        return getMonthlyPayment()*time;
    }

    /**
     * The method calculates the total interests.
     * @return totalInterests
     */
    public double getTotalInterest(){
        return getTotalPayment() - loanAmount;
    }

    private void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    private void setMonthlyRate(double monthlyRate) {
        this.monthlyRate = monthlyRate/(12*100);
    }

    private void setTime(double time) {
        this.time = time*12;
    }


}
