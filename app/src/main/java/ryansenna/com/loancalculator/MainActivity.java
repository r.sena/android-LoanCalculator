package ryansenna.com.loancalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import ryansenna.com.loancalculator.LoanCalculator;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    /**
     * The method resolves the calculations for a loan calculator when the Calculate Button
     * is pressed.
     * @param v
     */
    public void calculate(View v) {

        //get the values from the user
        EditText editText = (EditText) findViewById(R.id.loanAmount);
        double loanAmount = Double.parseDouble(editText.getText().toString());

        editText = (EditText) findViewById(R.id.rate);
        double monthRate = (Double.parseDouble(editText.getText().toString()));

        editText = (EditText) findViewById(R.id.years);
        double months = (Double.parseDouble(editText.getText().toString()));

        // calculate the month payment
        LoanCalculator lc = new LoanCalculator(loanAmount, monthRate, months);

        double monthlyPayment = lc.getMonthlyPayment();
        double totalPay = lc.getTotalPayment();
        double totalInterest = lc.getTotalInterest();

        //Display the results.
        display(monthlyPayment, totalPay, totalInterest);

    }

    /**
     * The method clears all the values entered and the answers when the clear button is
     * pressed.
     *
     * @param v
     */
    public void clear(View v) {
        EditText la = (EditText) findViewById(R.id.loanAmount);
        la.setText("" + 0);

        EditText ba = (EditText) findViewById(R.id.rate);
        ba.setText("" + 0);

        EditText ca = (EditText) findViewById(R.id.years);
        ca.setText("" + 0);

        TextView mPay = (TextView) findViewById(R.id.monthPay);
        mPay.setVisibility(View.INVISIBLE);

        TextView tPay = (TextView) findViewById(R.id.totalPay);
        tPay.setVisibility(View.INVISIBLE);

        TextView iPay = (TextView) findViewById(R.id.totalInterest);
        iPay.setVisibility(View.INVISIBLE);
    }

    /**
     * Displays all the values from the calculations.
     *
     * @param monthlyPay
     * @param totalPay
     * @param totalInt
     */
    private void display(double monthlyPay, double totalPay, double totalInt)
    {
        TextView mPay = (TextView) findViewById(R.id.monthPay);
        mPay.setText("" + monthlyPay);

        mPay.setVisibility(View.VISIBLE);

        TextView tPay = (TextView) findViewById(R.id.totalPay);
        tPay.setText("" + totalPay);
        tPay.setVisibility(View.VISIBLE);

        TextView iPay = (TextView) findViewById(R.id.totalInterest);
        iPay.setText("" + totalInt);
        iPay.setVisibility(View.VISIBLE);
    }
}
